import { Injectable } from '@angular/core';
import { Feathers } from './feathers.service';

/**
 *  Abstraction layer for data management.
 */
@Injectable()
export class DataService {
  constructor(private feathers: Feathers) {
  }

  things$() {
    // just returning the observable will query the backend on every subscription
    // using some caching mechanism would be wise in more complex applications
    return (<any>this.feathers // todo: remove 'any' assertion when feathers-reactive typings are up-to-date with buzzard
      .service('things'))
      .watch()
      .find();
  }
  
  cars$(){
    return (<any>this.feathers
    .service('cars'))
    .watch()
    .find();
  }
  
  addCar(make: string, model: string, year: number, mileage: number){
    if(make === ''){
      return;
    }
    if(model === ''){
      return;
    }
    if(year < 1913){ //assuming 1913 for Ford Model T as first sold car
      return;
    }
    if(mileage < 0){ //mileage could be 0, but not negative
      return;
    }
    this.feathers
    .service('cars')
    .create({
      make,
      model,
      year,
      mileage
    })
  }
  
  deleteCar(car){
    this.feathers
    .service('cars')
    .remove(car.id);
  }

  addThing(desc: string) {
    if (desc === '') {
      return;
    }

    // feathers-reactive Observables are hot by default,
    // so we don't need to subscribe to make create() happen.
    this.feathers
      .service('things')
      .create({
        desc
      });
  }
}
