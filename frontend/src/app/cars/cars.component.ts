import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

import { FormBuilder, Validators } from '@angular/forms';

import { Paginated } from '@feathersjs/feathers';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DataService } from '../services/data.service';


@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CarsComponent implements OnInit {
  
  cars$: Observable<any[]>;
  carForm;

  constructor(
    private data: DataService,
    private formBuilder: FormBuilder
  ) { 
    this.cars$ = data.cars$().pipe(
      map((t: Paginated<any>) => t.data)  
    );
  }

  submitCar(carData) {
    this.data.addCar( carData.make, carData.model, carData.year, carData.mileage );
  }

  deleteCar(car){
    console.log('deleting car'); 
    this.data.deleteCar(car);
    console.table(car);
  }
  ngOnInit(): void {
    this.carForm = this.formBuilder.group({
      make: ['', [Validators.required]],
      model: ['', [Validators.required]],
      year: ['', [Validators.required]],
      mileage: ['', [Validators.required]],
    })
  }

}
